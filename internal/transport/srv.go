package transport

import (
	"time"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	api "gitlab.com/avi-paint/proto/image-service-proto/go/v1/proc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
)

func NewImageProcServer() *grpc.Server {
	srv := grpc.NewServer(
		grpc.KeepaliveParams(keepalive.ServerParameters{
			Time:    50 * time.Second,
			Timeout: 10 * time.Second,
		}),
		grpc.KeepaliveEnforcementPolicy(keepalive.EnforcementPolicy{
			MinTime:             30 * time.Second,
			PermitWithoutStream: true,
		}),
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			unaryServerLogger,
			unaryServerRecover,
			unaryServerAccessLog,
		)),
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			streamServerLogger,
			streamServerRecover,
			streamServerAccessLog,
		)),
	)

	api.RegisterImageProcessingServiceServer(srv, &ImgProcHandler{})
	return srv
}
