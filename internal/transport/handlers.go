package transport

import (
	"context"

	api "gitlab.com/avi-paint/proto/image-service-proto/go/v1/proc"

	"gitlab.com/avi-paint/img-proc-service/internal/service"
)

type ImgProcHandler struct {
	api.UnimplementedImageProcessingServiceServer
}

func (h *ImgProcHandler) PyrMeanShiftFilter(ctx context.Context, in *api.PyrMeanShiftFilterRequest) (*api.DefaultReply, error) {
	_, err := service.PyrMeanShiftFilter(ctx, in)
	if err != nil {
		return nil, err
	}
	return &api.DefaultReply{OutPicture: []byte("test")}, nil
}

func (h *ImgProcHandler) DisplayMainContours(ctx context.Context, in *api.DisplayContoursRequest) (*api.DefaultReply, error) {
	_, err := service.DisplayContoursDefault(ctx, in)
	if err != nil {
		return nil, err
	}
	return &api.DefaultReply{OutPicture: []byte("test")}, nil
}
