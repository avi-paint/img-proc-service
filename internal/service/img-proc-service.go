package service

import (
	"context"

	api "gitlab.com/avi-paint/proto/image-service-proto/go/v1/proc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	ErrNotFound         = status.Error(codes.InvalidArgument, "ErrNotFound")
	ErrInvalidArguments = status.Error(codes.InvalidArgument, "ErrInvalidArguments")
)

func PyrMeanShiftFilter(ctx context.Context, in *api.PyrMeanShiftFilterRequest) (*api.DefaultReply, error) {
	return nil, nil
}

func DisplayContoursDefault(ctx context.Context, in *api.DisplayContoursRequest) (*api.DefaultReply, error) {
	return nil, nil
}
