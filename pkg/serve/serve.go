// Package serve provides helpers to start and shutdown neural_network services.
package serve

import (
	"context"
	"net"

	"github.com/powerman/go-service-example/pkg/netx"
	"github.com/powerman/structlog"
	"google.golang.org/grpc"

	"gitlab.com/avi-paint/img-proc-service/pkg/def"
)

func ServerGRPC(ctx context.Context, addr netx.Addr, srv *grpc.Server) error {
	log := structlog.FromContext(ctx, nil).New(def.LogServer, addr.String())

	listen, err := net.Listen("tcp", addr.String())
	if err != nil {
		return err
	}

	errc := make(chan error, 1)
	go func() { errc <- srv.Serve(listen) }()

	select {
	case err = <-errc:
	case <-ctx.Done():
		_ = srv.Stop
	}
	if err != nil {
		return log.Err("failed to serve grpc", "err", err)
	}
	return nil
}
