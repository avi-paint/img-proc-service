package utils

import (
	"encoding/json"
	"fmt"
	"github.com/Jeffail/gabs/v2"
	"reflect"
	"strings"
)

func StructToMap(obj interface{}) (newMap map[string]interface{}, err error) {
	data, err := json.Marshal(obj)

	if err != nil {
		return
	}
	err = json.Unmarshal(data, &newMap)
	return
}

//Put &Struct as params
func FillStruct(obj interface{}, params []interface{}) (b []byte, err error) {
	jsonObj := gabs.New()
	val := reflect.TypeOf(obj).Elem()

	for i := 0; i < len(params); i++ {
		_, err = jsonObj.Set(params[i], val.Field(i).Name)
	}
	b, err = json.Marshal(jsonObj)
	if err != nil {
		return nil, err
	}
	return b, err
}

//work if field mark as `json:"field"` and not supported int8...float64
func SetField(item interface{}, fieldName string, value interface{}) error {
	v := reflect.ValueOf(item).Elem()
	if !v.CanAddr() {
		return fmt.Errorf("cannot assign to the item passed, item must be a pointer in order to assign")
	}
	// It's possible we can cache this, which is why precompute all these ahead of time.
	findJsonName := func(t reflect.StructTag) (string, error) {
		if jt, ok := t.Lookup("json"); ok {
			return strings.Split(jt, ",")[0], nil
		}
		return "", fmt.Errorf("tag provided does not define a json tag %v", fieldName)
	}
	fieldNames := map[string]int{}
	for i := 0; i < v.NumField(); i++ {
		typeField := v.Type().Field(i)
		tag := typeField.Tag
		jName, _ := findJsonName(tag)
		fieldNames[jName] = i
	}

	fieldNum, ok := fieldNames[fieldName]
	if !ok {
		return fmt.Errorf("field %s does not exist within the provided item", fieldName)
	}
	fieldVal := v.Field(fieldNum)
	fieldVal.Set(reflect.ValueOf(value))
	return nil
}

func GetStructValues(obj interface{}) []interface{} {
	fields := reflect.TypeOf(obj)
	values := reflect.ValueOf(obj)

	result := make([]interface{}, 0)

	for i := 0; i < fields.NumField(); i++ {
		result = append(result, values.Field(i))
	}
	return result
}

func ToSlice(a interface{}) []interface{} {
	v := reflect.ValueOf(a)
	switch v.Kind() {
	case reflect.Slice, reflect.Array:
		result := make([]interface{}, v.Len())
		for i := 0; i < v.Len(); i++ {
			result[i] = v.Index(i).Interface()
		}
		return result
	default:
		return nil
	}
}