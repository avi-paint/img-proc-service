package utils

import (
	"encoding/csv"
	"fmt"
	"image"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"
)

type Info struct {
	Name    string
	Path    string
	IsDir   bool
	ModTime time.Time
	Size    int64
}

func GetListingDirectoryInfo(root string) ([]Info, error) {
	information := make([]Info, 0)
	err := filepath.Walk(root,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			information = append(information, Info{
				Name:    info.Name(),
				Path:    path,
				IsDir:   info.IsDir(),
				ModTime: info.ModTime(),
				Size:    info.Size(),
			})
			return nil
		})
	if err != nil {
		log.Println(err)
	}
	return information, err
}

func CreateFile(path string) (*os.File, error) {
	f, err := os.Create(path)
	if err != nil {
		log.Println("File not created:", path)
		return nil, err
	}
	return f, err
}

func OpenFile(fileInput string) (*os.File, error) {
	f, err := os.OpenFile(fileInput, os.O_RDWR, os.ModeAppend)
	if err != nil {
		log.Println("File not found:", fileInput)
		return nil, err
	}
	return f, err
}

func WriteFile(data []byte, path string) error {
	err := ioutil.WriteFile(path, data, 0644)
	if err != nil {
		log.Println("cannot write file:", path)
	}
	return err
}

func ReadFile(path string) ([]byte, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func ReadFileCSV(path string) ([][]string, error) {
	csvFile, err := OpenFile(path)
	if err != nil {
		return nil, err
	}
	defer csvFile.Close()

	csvLines, err := csv.NewReader(csvFile).ReadAll()
	if err != nil {
		log.Println("Cannot read records:", err)
	}
	return csvLines, err
}

func WriteFileCSV(path string, Data [][]string) error {
	csvFile, err := CreateFile(path)
	if err != nil {
		return err
	}
	defer csvFile.Close()

	writer := csv.NewWriter(csvFile)
	defer writer.Flush()

	for _, value := range Data {
		err := writer.Write(value)
		checkError("Cannot write to file", err)
	}
	return err
}

func LoadImage(fileInput string) (image.Image, error) {
	f, err := OpenFile(fileInput)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	img, _, err := image.Decode(f)
	if err != nil {
		return nil, err
	}
	return img, nil
}

func DownloadFile(filepath string, url string) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

func ParsePath(info Info) []string {
	result := strings.Split(info.Path, "\\")
	systemP := strings.Split(GetProjectDir(), "\\")
	return result[len(systemP)+1:]
}

func GetCurrentDir() (string, error) {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	return dir, err
}

func GetProjectDir() string {
	return "/Users/user/Documents/projects/imgprocservice"
}

func GetAssetsDir() string {
	return GetProjectDir() + "/assets"
}

func GetHtmlDir() string {
	return GetAssetsDir() + "/html"
}

func BuildImagePath(name string) string {
	path := fmt.Sprintf(GetAssetsDir()+"/examples/%s.jpg", name)
	return path
}
