package neural_network

import (
	"context"
	"paint/pkg/concurrent"
	"paint/pkg/neural_network/configs"
)

type Neural struct {
	cfg         *configs.ConfigNeural
	colorNeural *NeuralNetwork
}

func (n *Neural) runNeuralNetwork() (err error) {
	if n.cfg == nil {
		n.cfg, _ = configs.New()
	}
	err = concurrent.Setup(nil, map[interface{}]concurrent.SetupFunc{
		&n.colorNeural: n.runColorNeuralNetwork,
	})
	return err
}

func (n *Neural) runColorNeuralNetwork(ctx context.Context) (interface{}, error) {
	return n.InitColorNN()
}
