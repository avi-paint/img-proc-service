package neural_network

import (
	"encoding/json"
	"github.com/Jeffail/gabs/v2"
	"github.com/lucasb-eyer/go-colorful"
	"paint/pkg/combs"
	"paint/pkg/def"
	ips "paint/pkg/imageProcessingService"
	"paint/pkg/models"
	"paint/pkg/utils"
	"strconv"
	"strings"
)

const (
	subsetSize = 3
)

func (n *Neural) loadNN() (*NeuralNetwork, error) {
	colorNeuralN := LoadNN(n.cfg.ClrNN.Name)
	return colorNeuralN, nil
}

func (n *Neural) InitColorNN() (*NeuralNetwork, error) {
	colorNeuralN := DefaultNetwork(
		n.cfg.ClrNN.InputCount,
		n.cfg.ClrNN.HiddenCount,
		n.cfg.ClrNN.OutputCount,
		n.cfg.ClrNN.Regression)

	err := n.CreateTrainDataForNN()
	input, target, err := models.LoadData(n.cfg.ClrNN.InputCount, n.cfg.ClrNN.TrainData)

	colorNeuralN.Train(input, target, n.cfg.ClrNN.Iterations)
	DumpNN(n.cfg.ClrNN.Name, colorNeuralN)
	return colorNeuralN, err
}

func (n *Neural) CreateTrainDataForNN() (err error) {
	subsets := combs.Combinations(ips.ColorsHex(ips.MasterColors)[:5], subsetSize)
	for _, s := range subsets {
		color3 := ips.Color3{
			C1: s[0],
			C2: s[1],
			C3: s[2],
		}
		blending := ips.Blend3Colors(color3, ips.DefaultNumberOfShades)
		err = n.insertTrainDataForNN(color3, blending)
		if err != nil {
			return err
		}
	}
	return err
}

func (n *Neural) insertTrainDataForNN(c ips.Color3, blendResult *gabs.Container) error {
	dataCSV := make([]models.DataCSV, 0)

	for C2, child := range blendResult.S(c.C1).ChildrenMap() {
		for _, value := range child.ChildrenMap() {
			var blendColors map[string]interface{}
			_ = json.Unmarshal([]byte(value.String()), &blendColors)

			//C2 format is 0.5#hexColor
			split := strings.Split(C2, def.Lattice)
			ShadeC2, _ := strconv.ParseFloat(split[0], 64)

			for shade, v := range blendColors {

				colorR, _ := colorful.Hex(v.(string))
				lR, uR, vR := colorR.Luv()

				inputData := models.InputData{
					LR: lR,
					UR: uR,
					VR: vR,
				}

				ShadeC3, _ := strconv.ParseFloat(shade, 64)
				colorC1, _ := colorful.Hex(c.C1)
				lC1, uC1, vC1 := colorC1.Luv()

				colorC2, _ := colorful.Hex(c.C2)
				lC2, uC2, vC2 := colorC2.Luv()

				colorC3, _ := colorful.Hex(c.C3)
				lC3, uC3, vC3 := colorC3.Luv()

				targetData := models.OutData{
					LC1:     lC1,
					UC1:     uC1,
					VC1:     vC1,
					ShadeC2: ShadeC2,
					LC2:     lC2,
					UC2:     uC2,
					VC2:     vC2,
					ShadeC3: ShadeC3,
					LC3:     lC3,
					UC3:     uC3,
					VC3:     vC3,
				}

				dataCSV = append(dataCSV, models.DataCSV{
					InputData:  inputData,
					TargetData: targetData,
				})
			}
		}
	}
	return models.SaveData(dataCSV, utils.GetNeuralDir()+"\\"+n.cfg.ClrNN.TestData)
}
