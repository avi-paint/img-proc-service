module gitlab.com/avi-paint/img-proc-service

go 1.17

require (
	github.com/Jeffail/gabs/v2 v2.6.1
	github.com/cenkalti/backoff/v4 v4.1.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.10.4
	github.com/lucasb-eyer/go-colorful v1.2.0
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/oliamb/cutter v0.2.2
	github.com/pkg/errors v0.9.1
	github.com/powerman/getenv v0.1.0
	github.com/powerman/go-service-example v1.4.0
	github.com/powerman/must v0.1.1
	github.com/powerman/structlog v0.7.3
	github.com/pressly/goose v2.7.0+incompatible
	github.com/prometheus/client_golang v1.11.0
	github.com/rohanthewiz/element v0.2.2
	github.com/streadway/amqp v1.0.0
	gitlab.com/avi-paint/proto/image-service-proto v1.0.6
	gocv.io/x/gocv v0.29.0
	google.golang.org/grpc v1.42.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.1 // indirect
	github.com/golang/protobuf v1.5.0 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/prometheus/client_model v0.2.0 // indirect
	github.com/prometheus/common v0.26.0 // indirect
	github.com/prometheus/procfs v0.6.0 // indirect
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/sys v0.0.0-20210603081109-ebe580a85c40 // indirect
	golang.org/x/text v0.3.5 // indirect
	google.golang.org/genproto v0.0.0-20200707001353-8e8330bf89df // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
